package com.example.inky.openweatherapitest;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity
{

    public class DownloadWeatherTask extends AsyncTask<String,Void,String>
    {
        @Override
        protected String doInBackground(String... strings)
        {
            String result="";

            URL url;
            HttpURLConnection conn = null;
            try
            {
                url = new URL(strings[0]);

                conn = (HttpURLConnection) url.openConnection();
                InputStream in = conn.getInputStream();
                InputStreamReader reader = new InputStreamReader(in);
                int data = reader.read();

                while (data != -1)
                {
                    char current = (char) data;
                    result+=current;
                    data = reader.read();
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                return "Failed";
            }

            return result;
        }

        @Override
        protected void onPostExecute(String result)
        {
            super.onPostExecute(result);

            try
            {
                JSONObject jsonObject = new JSONObject(result);
                String weatherInfo = jsonObject.getString("weather");
                Log.i("Weather", weatherInfo);
                JSONArray arr = new JSONArray(weatherInfo);
                Log.i("Name", jsonObject.getString("name"));

                for(int i =0; i < arr.length(); i++)
                {
                    JSONObject jsonpart = arr.getJSONObject(i);
                    Log.i("main", jsonpart.getString("main"));
                    Log.i("description", jsonpart.getString("description"));
                }

            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }


        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        DownloadWeatherTask wTask = new DownloadWeatherTask();
        wTask.execute("http://api.openweathermap.org/data/2.5/weather?q=Zermatt&&APPID=a059c97a5ffc910ca81e234ee45fda8d");



    }
}
